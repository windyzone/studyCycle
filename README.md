# 企业远程教育培训平台

#### 介绍  http://114.132.231.148:50101/oioi/education/741/index.html
平台定位为：“内容+社区+企业”的发展模式，提供在线教学与企业员工培训的全流程管理，教育培训资源的管理，通过对信息行业相关课程资源的垂直整合，最终打造成为一个“教学平台+企业培训平台+资源库”的“互联网+职业培训”在线学习系统，以网络学习的方式服务于职业教育和企业员工培训，本平台以WEB方式提供服务，依据学习资源和学习场景的不同，提供多个学习模块功能，为不同用户对象提供不同操作空间及入口。为学生会员及企业会员提供互动式学习服务和积分回报奖励，以更便捷地方式、更实惠的价格和更方便友好的体验提供更多个性化学习课程。

- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/155908_1c621d63_748706.png "搜狗截图20年02月19日1601_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160015_46167040_748706.png "搜狗截图20年02月19日1603_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160127_3f4fe84f_748706.png "搜狗截图20年02月19日1604_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160216_9ca61589_748706.png "搜狗截图20年02月19日1605_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160242_f321aab5_748706.png "搜狗截图20年02月19日1605_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160304_ce0e69e8_748706.png "搜狗截图20年02月19日1606_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160342_30c4a79e_748706.png "搜狗截图20年02月19日1606_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160434_649e2f74_748706.png "搜狗截图20年02月19日1607_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160456_a350d06b_748706.png "搜狗截图20年02月19日1608_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160522_452ca4f6_748706.png "搜狗截图20年02月19日1608_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160603_3aa9cbc5_748706.png "搜狗截图20年02月19日1609_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160629_c86f0118_748706.png "搜狗截图20年02月19日1609_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160651_81ad4d27_748706.png "搜狗截图20年02月19日1610_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160723_2d1100f3_748706.png "搜狗截图20年02月19日1610_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160805_b154a6e0_748706.png "搜狗截图20年02月19日1611_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160827_b1be680a_748706.png "搜狗截图20年02月19日1611_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160858_16a6ee0f_748706.png "搜狗截图20年02月19日1612_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/160923_73b77986_748706.png "搜狗截图20年02月19日1612_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161000_ee718d56_748706.png "搜狗截图20年02月19日1613_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161024_97ed51d4_748706.png "搜狗截图20年02月19日1613_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161049_61298b8b_748706.png "搜狗截图20年02月19日1614_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161111_8d79bb1b_748706.png "搜狗截图20年02月19日1614_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161139_9d99c48f_748706.png "搜狗截图20年02月19日1614_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161201_e024a015_748706.png "搜狗截图20年02月19日1615_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161223_4b17265c_748706.png "搜狗截图20年02月19日1615_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161254_5c8e7443_748706.png "搜狗截图20年02月19日1616_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161323_ba361267_748706.png "搜狗截图20年02月19日1616_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161345_3417a7a1_748706.png "搜狗截图20年02月19日1616_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161414_d5eacf30_748706.png "搜狗截图20年02月19日1617_1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0219/161437_58bbc13f_748706.png "搜狗截图20年02月19日1617_1.png")


### 1.学生Protal门户网站
### 1.1.体验地址

 在浏览器地址栏输入：http://114.132.231.148:50100/study 进入首页
 体验账户
      账户：zxstudent1 
      密码：123456

